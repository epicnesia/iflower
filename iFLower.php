<?php
/**
 * @package iFlower
 * @version 1.0
 */
/*
Plugin Name: iFlower
Plugin URI: http://iflow-pare.com
Description: This is the online registration system of iFlow Pare
Author: Taufik Fredi Pratama
Version: 1.0
Author URI: http://taufikfredipratama.info
*/

require_once('inc/cmb2.iflower.php');

global $iflower_db_install;
$iflower_db_install = '0.1';

// Database setup
function iflower_db_install(){
	global $wpdb;

	$table_name = "iflower_student_data";
	$charset_collate = $wpdb->get_charset_collate();
	
	$sql = "CREATE TABLE IF NOT EXISTS $table_name (
	  id int(11) NOT NULL AUTO_INCREMENT,
	  register_time datetime DEFAULT CURRENT_TIMESTAMP NOT NULL,
	  name varchar(200) NOT NULL,
	  school_from varchar(200) NOT NULL,
	  gender tinyint(2) NOT NULL,
	  birth_data varchar(500) NOT NULL,
	  address text NOT NULL,
	  phone varchar(20) NOT NULL,
	  email varchar(500) NOT NULL,
	  period date NOT NULL,
	  program_1 int(11) NOT NULL,
	  program_price_1 int(11) NOT NULL,
	  program_2 int(11) NULL,
	  program_price_2 int(11) NULL,
	  camp int(11) NOT NULL,
	  camp_price int(11) NULL,
	  invoice_status tinyint(2) DEFAULT '0' NOT NULL,
	  confirmation_status tinyint(2) DEFAULT '0' NOT NULL,
	  PRIMARY KEY  (id)
	) $charset_collate;";
	
	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	dbDelta( $sql );
	
}
register_activation_hook( __FILE__, 'iflower_db_install' );

// Form Templating
function iflower_page_template( $page_template )
{
    if ( is_page( 'online-registration' ) ) {
    	session_start();
		
		if (!isset($_SESSION['private_key'])) {
			$_SESSION['private_key'] = sha1(time());
		}
		
        $page_template = dirname( __FILE__ ) . '/form.iflower.php';
    } elseif ( is_page( 'registration-success' ) ) {
		
        $page_template = dirname( __FILE__ ) . '/inc/success.iflower.php';
        
    } elseif ( is_page( 'registration-failed' ) ) {
		
        $page_template = dirname( __FILE__ ) . '/inc/failed.iflower.php';
        
    }
    return $page_template;
}
add_filter( 'page_template', 'iflower_page_template' );

// Registering Scripts
function iflower_scripts(){
	// Register the style
    wp_register_style( 'iflower-style', plugins_url( '/inc/iflower.css', __FILE__ ), array(), '1.0', 'all' );
	// Register the validation script
    wp_register_script( 'iflower-validation-script', plugins_url( '/inc/jquery.validate.min.js', __FILE__ ), array(), '1.15.0', true );
	// Register the script
    wp_register_script( 'iflower-script', plugins_url( '/inc/iflower.js', __FILE__ ), array(), '1.0', true );
	// Register the recaptcha
    //wp_register_script( 'iflower-recaptcha', 'https://www.google.com/recaptcha/api.js', array() );
}
add_action('init', 'iflower_scripts');

// Enqueue scripts
function enqueue_scripts(){
	if ( is_page( 'online-registration' ) ) {
		wp_enqueue_style('iflower-style');
		wp_enqueue_script('iflower-validation-script');
		//wp_enqueue_script('iflower-recaptcha');
		wp_enqueue_script('iflower-script');
		wp_localize_script( 'iflower-script', 'iflower', array(
			'ajax_url' => admin_url( 'admin-ajax.php' )
		));
	}
}
add_action('wp_enqueue_scripts', 'enqueue_scripts', 11);

// Function to handle iflower AJAX request
add_action( 'wp_ajax_nopriv_get_period_posts', 'get_period_posts' );
add_action( 'wp_ajax_get_period_posts', 'get_period_posts' );

function get_period_posts() {
	
	if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
		
		$the_query = null;
		$response = '<option value="0"> -- Pilih Program -- </option>';
		
		// The Query
		if($_REQUEST['period'] > 0){
		 
			$the_query = new WP_Query( array( 'posts_per_page' => '-1', 'meta_key' => '_iflower_period', 'meta_value' => $_REQUEST['period'], 'orderby' => 'name', 'order' => 'ASC' ) );
			
		}
			
		// The Loop
		if ( !is_null($the_query) ) {
				
			while ( $the_query->have_posts() ) {
				$the_query->the_post();
				$response .= '<option value="' . get_the_ID() . '"> ' . get_the_title() . ' </option>';
			}
				
			wp_reset_postdata();
		} 
		
		echo $response;
			
	}

	die();
	
}

add_action( 'wp_ajax_nopriv_get_price', 'get_price' );
add_action( 'wp_ajax_get_price', 'get_price' );

function get_price() {
	
	if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
		
		$response = array('formated' => 'IDR ' . number_format('0', 0, ',', '.') . ',-', 'raw'=> '0');
		
		// The Query
		if($_REQUEST['id'] > 0){
		 
			$price = get_post_meta($_REQUEST['id'], '_iflower_price', true);
			
			$response = array('formated' => 'IDR ' . number_format($price, 0, ',', '.') . ',-', 'raw'=> $price);
			
		}
		
		echo json_encode($response);
			
	}

	die();
	
}

?>