<?php
/**
 * Template Name: Success iFLower Page
 *
 * Description: A custom page template for displaying congratulation speech when registration success.
 *
 * @package iFlow
 * @since 1.0
 */
?>

<?php get_header(); ?>

<section id="content">
	<?php 
		
		if (have_posts()) : while (have_posts()) : the_post(); 

	?>
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<center>
					<h1 class="single-post-title">Selamat, Registrasi Sukses</h1>
					<div class="single-post-title-divider"></div>
				</center>
				<?php the_content(); ?>
			</div>
		</div>
	</div>
	<?php
		endwhile;
		endif;
	?>
</section>

<?php get_footer(); ?>