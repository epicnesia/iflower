$(document).ready(function() {

	//============================= iFLower Scripts ==================================
	$("#regPeriodDate").change(function() {

		$.ajax({
			url : iflower.ajax_url,
			type : 'post',
			data : {
				action : 'get_period_posts',
				period : $(this).val()
			},
		}).done(function(r) {
			$("#regProgram1").html(r);
			$("#regProgram2").html(r);
		});

	});

	$("#regProgram1").change(function() {

		$.ajax({
			url : iflower.ajax_url,
			type : 'post',
			dataType : 'json',
			data : {
				action : 'get_price',
				id : $(this).val()
			},
		}).done(function(r) {
			$("#regPrice1").html(r.formated);
			$("input[name='regPrice1']").val(r.raw);
			total_price();
		});

	});

	$("#regProgram2").change(function() {

		$.ajax({
			url : iflower.ajax_url,
			type : 'post',
			dataType : 'json',
			data : {
				action : 'get_price',
				id : $(this).val()
			},
		}).done(function(r) {
			$("#regPrice2").html(r.formated);
			$("input[name='regPrice2']").val(r.raw);
			total_price();
		});

	});

	$("input[name='regCamp']").change(function() {

		if ($("input[name='regCamp']:checked").val() == 1) {
			$("#regCampDuration").slideDown('slow');
		} else {
			$("input[name='regCampPrice']:checked").removeAttr('checked');
			$("#regCampDuration").slideUp('slow');
		}

		total_price();

	});

	$("input[name='regCampPrice']").change(function() {

		total_price();

	});

	function total_price() {

		var regPrice1 = $("input[name='regPrice1']").val() > 0 ? parseInt($("input[name='regPrice1']").val()) : parseInt(0);
		var regPrice2 = $("input[name='regPrice2']").val() > 0 ? parseInt($("input[name='regPrice2']").val()) : parseInt(0);
		var regCampPrice = $("input[name='regCampPrice']:checked").val() > 0 ? parseInt($("input[name='regCampPrice']:checked").val()) : parseInt(0);

		var totalPrice = regPrice1 + regPrice2 + regCampPrice;

		$("#totalPrice").html('IDR ' + number_format(totalPrice, 0, ',', '.') + ',-');

	}

	function number_format(number, decimals, dec_point, thousands_sep) {
		// Strip all characters but numerical ones.
		number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
		var n = !isFinite(+number) ? 0 : +number,
		    prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
		    sep = ( typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
		    dec = ( typeof dec_point === 'undefined') ? '.' : dec_point,
		    s = '',
		    toFixedFix = function(n, prec) {
			var k = Math.pow(10, prec);
			return '' + Math.round(n * k) / k;
		};
		// Fix for IE parseFloat(0.55).toFixed(0) = 0;
		s = ( prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
		if (s[0].length > 3) {
			s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
		}
		if ((s[1] || '').length < prec) {
			s[1] = s[1] || '';
			s[1] += new Array(prec - s[1].length + 1).join('0');
		}
		return s.join(dec);
	}

	// Form validation
	$(function() {
		$("form#form-registration").validate({
			// Specify validation rules
			rules : {
				regName : "required",
				regSchoolFrom : "required",
				regGender : {
					required : true,
					min : 1
				},
				regBirthdata : "required",
				regAddress : "required",
				regPhone : {
					required : true,
					min : 1
				},
				regMail : {
					required : true,
					email : true
				},
				regPeriodDate : {
					required : true,
					min : 1
				},
				regPeriodMonth : {
					required : true,
					min : 1
				},
				regPeriodYear : {
					required : true,
					min : 1
				},
				regProgram1 : {
					required : true,
					min : 1
				},
				regPrice1 : {
					required : true,
					min : 1
				},
				regCamp : {
					required : true,
					min : 1
				},
			},
			// Specify validation error messages
			messages : {
				regName : "Nama wajib diisi",
				regSchoolFrom : "Sekolah Asal wajib diisi",
				regGender : {
					required : "Gender wajib diisi",
					min : "Gender wajib diisi"
				},
				regBirthdata : "Tempat tanggal lahir wajib diisi",
				regAddress : "Alamat wajib diisi",
				regPhone : {
					required : "Phone wajib diisi",
					min : "Phone wajib diisi"
				},
				regMail : {
					required : "Email wajib diisi",
					email : "Email wajib diisi dengan alamat email yang valid"
				},
				regPeriodDate : {
					required : "Tanggal periode wajib diisi",
					min : "Tanggal periode wajib diisi"
				},
				regPeriodMonth : {
					required : "Bulan periode wajib diisi",
					min : "Bulan periode wajib diisi"
				},
				regPeriodYear : {
					required : "Tahun periode wajib diisi",
					min : "Tahun periode wajib diisi"
				},
				regProgram1 : {
					required : "Program wajib diisi",
					min : "Program wajib diisi"
				},
				regPrice1 : {
					required : "Program wajib diisi",
					min : "Program wajib diisi"
				},
				regCamp : {
					required : "Camp wajib diisi",
					min : "Camp wajib diisi"
				},
			},
			// Make sure the form is submitted to the destination defined
			// in the "action" attribute of the form when valid
			submitHandler : function(form) {
				form.submit();
			}
		});
	});

});
