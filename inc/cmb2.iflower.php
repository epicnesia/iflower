<?php

/**
 * Get the cmb2 bootstrap!
 */
if ( file_exists(  __DIR__ . '/cmb2/init.php' ) ) {
  require_once  __DIR__ . '/cmb2/init.php';
} elseif ( file_exists(  __DIR__ . '/CMB2/init.php' ) ) {
  require_once  __DIR__ . '/CMB2/init.php';
}

add_action( 'cmb2_admin_init', 'iflower_custom_fields' );
/**
 * Define the metabox and field configurations.
 */
function iflower_custom_fields() {

    // Start with an underscore to hide fields from custom fields list
    $prefix = '_iflower_';

    /**
     * Initiate the metabox
     */
    $cmb = new_cmb2_box( array(
        'id'            => 'iFlower_data',
        'title'         => __( 'Online Registration Meta Data', 'cmb2' ),
        'object_types'  => array( 'post', ), // Post type
        'context'       => 'normal',
        'priority'      => 'high',
        'show_names'    => true, // Show field names on the left
        // 'cmb_styles' => false, // false to disable the CMB stylesheet
        // 'closed'     => true, // Keep the metabox closed by default
    ) );
	
	// Period
    $cmb->add_field( array(
		'name'             => __( 'Period', 'cmb2' ),
		'id'               => $prefix . 'period',
		'type'             => 'multicheck',
		'multiple' 		   => true,
		'options'          => array(
			'10' 	=> __( '10', 'cmb2' ),
			'25'   	=> __( '25', 'cmb2' ),
		),
	) );

    // URL text field
    $cmb->add_field( array(
        'name' => __( 'Biaya', 'cmb2' ),
        'id'   => $prefix . 'price',
        'type' => 'text',
    ) );

}

?>