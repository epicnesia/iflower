<?php
session_start();
require_once('inc/gump.class.php');
include "../../../wp-config.php";

date_default_timezone_set('Asia/Jakarta');

$db = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
// Test the connection:
if ($db->connect_errno){
    // Connection Error
    exit("Couldn't connect to the database: ".$db->connect_error);
}

add_filter( 'wp_mail_content_type', 'wpdocs_set_html_mail_content_type' );
function wpdocs_set_html_mail_content_type() {
    return 'text/html';
}

// Prevent CSRF
//if(isset($_POST['submit']) && $_POST['secret_key'] == $_SESSION['private_key']){

	$gump = new GUMP(); // init GUMP to validate
	
	$_POST = $gump->sanitize($_POST); // Sanitize by GUMP
	
	// Set validation rules
	$gump->validation_rules(array(
	    'regName'    	=> 'required|valid_name',
	    'regSchoolFrom' => 'required|alpha_space',
	    'regGender'     => 'required|numeric',
	    'regBirthdata'  => 'required',
	    'regAddress' 	=> 'required',
	    'regPhone' 		=> 'required',
	    'regMail' 		=> 'required|valid_email',
	    'regPeriodDate' => 'required|numeric',
	    'regPeriodMonth'=> 'required|numeric',
	    'regPeriodYear' => 'required|numeric',
	    'regProgram1' 	=> 'required|numeric',
	    'regPrice1' 	=> 'required|numeric',
	    'regProgram2' 	=> 'numeric',
	    'regPrice2' 	=> 'numeric',
	    'regCamp' 		=> 'required|numeric',
	    'regCampPrice' 	=> 'numeric',
	));
	
	// Set filter rules
	$gump->filter_rules(array(
	    'regName'    	=> 'trim|sanitize_string',
	    'regSchoolFrom' => 'trim|sanitize_string',
	    'regGender'     => 'trim|sanitize_string|sanitize_numbers',
	    'regBirthdata'  => 'trim|sanitize_string',
	    'regAddress' 	=> 'trim|sanitize_string',
	    'regPhone' 		=> 'trim|sanitize_string',
	    'regMail' 		=> 'trim|sanitize_string|sanitize_email',
	    'regPeriodDate' => 'trim|sanitize_string|sanitize_numbers',
	    'regPeriodMonth'=> 'trim|sanitize_string|sanitize_numbers',
	    'regPeriodYear' => 'trim|sanitize_string|sanitize_numbers',
	    'regProgram1' 	=> 'trim|sanitize_string|sanitize_numbers',
	    'regPrice1' 	=> 'trim|sanitize_string|sanitize_numbers',
	    'regProgram2' 	=> 'trim|sanitize_string|sanitize_numbers',
	    'regPrice2' 	=> 'trim|sanitize_string|sanitize_numbers',
	    'regCamp' 		=> 'trim|sanitize_string|sanitize_numbers',
	    'regCampPrice' 	=> 'trim|sanitize_string|sanitize_numbers',
	));
	
	// Set field name, I'm sorry for change error messages by hardcoding
	$set_name = array(
		'regName'    	=> 'Nama',
	    'regSchoolFrom' => 'Asal Sekolah',
	    'regGender'     => 'Gender',
	    'regBirthdata'  => 'Tempat Tanggal Lahir',
	    'regAddress' 	=> 'Alamat',
	    'regPhone' 		=> 'Phone',
	    'regMail' 		=> 'Email',
	    'regPeriodDate' => 'Tanggal Periode',
	    'regPeriodMonth'=> 'Bulan Periode',
	    'regPeriodYear' => 'Tahun Periode',
	    'regProgram1' 	=> 'Program 1',
	    'regPrice1' 	=> 'Program 1',
	    'regProgram2' 	=> 'Program 2',
	    'regPrice2' 	=> 'Program 2',
	    'regCamp' 		=> 'Camp',
	    'regCampPrice' 	=> 'Durasi Camp',
		);
		
	foreach ($set_name as $key => $value) {
		$gump->set_field_name($key, $value);
	}
	
	// Run Validation
	$validated_data = $gump->run($_POST);
	
	if($validated_data === false) {
		
		$_SESSION['private_key'] = sha1(time());
	    $_SESSION['errors'] = $gump->get_readable_errors();
		$_SESSION['form_values'] = $_POST;
		
		redirect($_SERVER["HTTP_REFERER"]); // Throw back if fail to validate
		
	} elseif($validated_data['regCamp'] == 1 && (!isset($validated_data['regCampPrice']) || empty($validated_data['regCampPrice']))){
		$_SESSION['private_key'] = sha1(time());
		$_SESSION['errors'] = array('Anda belum memilih durasi camp!');
		$_SESSION['form_values'] = $_POST;
			
		redirect($_SERVER["HTTP_REFERER"]); // Throw back if fail to validate
	} else {
		
		$timestamp = date('Y-m-d H:i:s');
		$program2 = isset($validated_data['regProgram2']) && !empty($validated_data['regProgram2']) ? $validated_data['regProgram2'] : null;
		$program_price2 = isset($validated_data['regPrice2']) && !empty($validated_data['regPrice2']) ? $validated_data['regPrice2'] : null;
		$camp_price = isset($validated_data['regCampPrice']) && !empty($validated_data['regCampPrice']) ? $validated_data['regCampPrice'] : '';
		
		// Insert query
		$sql = "INSERT INTO iflower_student_data (register_time, name, school_from, gender, birth_data, address,
						phone, email, period, program_1, program_price_1, program_2, program_price_2, camp, camp_price) 
						VALUES ('$timestamp', '$validated_data[regName]', '$validated_data[regSchoolFrom]', '$validated_data[regGender]',
						'$validated_data[regBirthdata]', '$validated_data[regAddress]', '$validated_data[regPhone]', '$validated_data[regMail]',
						'$validated_data[regPeriodYear]-$validated_data[regPeriodMonth]-$validated_data[regPeriodDate]',
						'$validated_data[regProgram1]', '$validated_data[regPrice1]', '$program2', '$program_price2', '$validated_data[regCamp]', '$camp_price')";
		
		// Executing insert
		if ($db->query($sql) === TRUE) {
			
			sendInvoice($validated_data['regMail'], $validated_data);
			// Success
		    redirect(esc_url(home_url().'/registration-success'));
		} else {
			// Error
		    redirect(esc_url(home_url().'/registration-failed'));
		}
		
		$db->close();
		
		// Clean up session
		unset($_SESSION['private_key']);
		unset($_SESSION['form_values']);
		
	}

//}

// Function to redirect url
function redirect($url){
    $string = '<script type="text/javascript">';
    $string .= 'window.location = "' . $url . '"';
    $string .= '</script>';
    echo $string;
}

function sendInvoice($to, $detail = array())
{
	
	$gender = $detail['regGender'] == 1 ? 'Pria' : 'Wanita';
	$monthPeriod = array('01'=>'Januari', '02'=>'Februari', '03'=>'Maret', '04'=>'April', '05'=>'Mei', '06'=>'Juni', '07'=>'Juli', '08'=>'Agustus', '09'=>'September', '10'=>'Oktober', '11'=>'November', '12'=>'Desember');
	$program1 = isset($detail['regProgram1']) && !empty($detail['regProgram1']) ? get_the_title($detail['regProgram1']) : '-';
	$program_price1 = isset($detail['regPrice1']) && !empty($detail['regPrice1']) ? 'IDR '.number_format($detail['regPrice1'], 0, ',', '.') . ',-' : '-';
	$raw_program_price1 = isset($detail['regPrice1']) && !empty($detail['regPrice1']) ? $detail['regPrice1'] : 0;
	$program2 = isset($detail['regProgram2']) && !empty($detail['regProgram2']) ? get_the_title($detail['regProgram2']) : '-';
	$program_price2 = isset($detail['regPrice2']) && !empty($detail['regPrice2']) ? 'IDR '.number_format($detail['regPrice2'], 0, ',', '.') . ',-' : '-';
	$raw_program_price2 = isset($detail['regPrice2']) && !empty($detail['regPrice2']) ? $detail['regPrice2'] : 0;
	$camp = $detail['regCamp'] == 1 ? 'Ya' : 'Tidak';
	$camp_price = isset($detail['regCampPrice']) && !empty($detail['regCampPrice']) ? 'IDR '.number_format($detail['regCampPrice'], 0, ',', '.') . ',-' : '-';
	$raw_camp_price = isset($detail['regCampPrice']) && !empty($detail['regCampPrice']) ? $detail['regCampPrice'] : 0;
	$total_price = $raw_program_price1 + $raw_program_price2 + $raw_camp_price;
 
	$subject = 'Pendaftaran Online iFLow Pare';
	$body = '
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Terima Kasih telah bergabung dengan kami.</title>
	</head>
	<body yahoo bgcolor="#ffffff">
		<table width="100%" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td>
				<!--[if (gte mso 9)|(IE)]>
				<table width="600" align="center" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td>
						<![endif]-->
						<table class="content" align="center" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td bgcolor="#ffffff">
									<table align="center" border="0" cellpadding="0" cellspacing="0">
									    <tr>
									        <td style="width: 100%; padding: 20px 0;">
									            <img src="http://iflow-pare.com/wp-content/themes/iflow/img/logo-mini.png" width="113" height="70" border="0" alt="" / >
									        </td>
									    </tr>
									</table>
								</td>
							</tr>
							<tr>
								<td bgcolor="#ffffff">
									<table width="100%" align="left" border="0" cellpadding="0" cellspacing="0">
									    <tr>
									        <td>
									            <hr width="100%" />
									        </td>
									    </tr>
									</table>
								</td>
							</tr>
							<tr>
								<td bgcolor="#ffffff">
									<table width="100%" align="left" border="0" cellpadding="0" cellspacing="0">
									    <tr>
									        <td>
									            <h3>Terimakasih karena telah bergabung dengan iFLow Pare</h3>
									            <p>Berikut ini adalah detail dari registrasi Anda</p>
									            <ol>
									            	<li style="padding:5px 0;">Nama : '.$detail['regName'].'</li>
									            	<li style="padding:5px 0;">Sekolah Asal : '.$detail['regSchoolFrom'].'</li>
									            	<li style="padding:5px 0;">Gender : '.$gender.'</li>
									            	<li style="padding:5px 0;">Tempat, Tanggal Lahir : '.$detail['regBirthdata'].'</li>
									            	<li style="padding:5px 0;">Alamat : '.$detail['regAddress'].'</li>
									            	<li style="padding:5px 0;">Phone / HP : '.$detail['regPhone'].'</li>
									            	<li style="padding:5px 0;">Email : '.$detail['regMail'].'</li>
									            	<li style="padding:5px 0;">Periode : '.$detail['regPeriodDate'].' '.$monthPeriod[$detail['regPeriodMonth']].' '.$detail['regPeriodYear'].'</li>
									            	<li style="padding:5px 0;">Program 1 : '.$program1.'</li>
									            	<li style="padding:5px 0;">Biaya Program 1 : '.$program_price1.'</li>
									            	<li style="padding:5px 0;">Program 2 : '.$program2.'</li>
									            	<li style="padding:5px 0;">Biaya Program 2 : '.$program_price2.'</li>
									            	<li style="padding:5px 0;">Camp : '.$camp.'</li>
									            	<li style="padding:5px 0;">Biaya Camp : '.$camp_price.'</li>
									            </ol>
									            <h3><strong>Total Biaya : IDR '.number_format($total_price, 0, ',', '.').',-</strong></h3>
									        </td>
									    </tr>
									    <tr>
									        <td>
									        	<hr width="100%" />
									            <h4>TERMS AND CONDITIONS</h4>
									            <ol>
									            	<li style="padding:10px 0;">Program yang sudah dibayarkan tidak dapat dibatalkan, namun dapat diganti dengan program lain sesuai dengan standard harga yang sudah ada.</li>
									            	<li style="padding:10px 0;">Batas waktu penggantian program maksimal 48 jam setelah kelas pertama program dimulai.</li>
									            	<li style="padding:10px 0;">Transfer fee sekaligus konfirmasi maksimal 2 hari sebelum kelas pertama program dimulai. Jika dalam waktu tersebut transfer belum dilakukan maka calon peserta dianggap membatalkan program.</li><li>Batas waktu penggantian program maksimal 48 jam setelah kelas pertama program dimulai.</li>
									            	<li  style="padding:10px 0;">
									            		Program Fee dapat di transfer ke rekening berikut:<br />
									            		<ul>
									            			<li style="padding:5px 0;">
									            				<strong>Bank BCA</strong>
									            			</li>
									            			<li style="padding:5px 0;">
									            				<strong>No.  Rek. 1400582170</strong>
									            			</li>
									            			<li style="padding:5px 0;">
									            				<strong>Atas Nama : SUPIJANTO</strong>
									            			</li>
									            		</ul>
									            	</li>
									            	<li style="padding:10px 0;">
									            		Konfirmasi Transfer dapat dilakukan dengan cara berikut:
									            		<ul>
									            			<li style="padding:5px 0;">Ketik	: tanggal transfer/nama/nomor rekening/jumlah yang ditransfer</li>
									            			<li style="padding:5px 0;">Contoh	: <strong>22082016/Indah Permatasari/87654321/550000</strong></li>
									            			<li style="padding:5px 0;">Kirim ke: <strong>0857 0403 8090</strong></li>
									            		</ul>
									            	</li>
									            	<li style="padding:10px 0;">Jadwal kelas masing-masing program dapat dilihat di <a href="http://iflow-pare.com/">www.iflow–pare.com</a> atau di main office iflow.</li>
									            </ol>
									        </td>
									    </tr>
									</table>
								</td>
							</tr>
						</table>
						<!--[if (gte mso 9)|(IE)]>
						</td>
					</tr>
				</table>
				<![endif]-->
				</td>
			</tr>
		</table>
	</body>
</html>';
	 
	wp_mail( $to, $subject, $body );
	 
	// Reset content-type to avoid conflicts -- https://core.trac.wordpress.org/ticket/23578
	remove_filter( 'wp_mail_content_type', 'wpdocs_set_html_mail_content_type' );
	
}