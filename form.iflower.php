<?php
/**
 * Template Name: Form iFLower Page
 *
 * Description: A custom page template for displaying online registration form.
 *
 * @package iFlow
 * @since 1.0
 */
?>

<?php get_header(); ?>

<section id="content">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<center>
					<h1 class="single-post-title">Form Registrasi</h1>
					<div class="single-post-title-divider"></div>
				</center>
				<?php 
				
					if(isset($_SESSION['errors']) && !empty($_SESSION['errors'])){
						
						foreach ($_SESSION['errors'] as $error) {
							
							?>
							
							<div class="alert alert-warning alert-dismissible" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<strong>Warning!</strong> <?php echo $error; ?>
							</div>
							
							<?php
							
						}
						
						unset($_SESSION['errors']);
						
					}
				
				?>
				<form id="form-registration" class="form-horizontal" action="<?php echo esc_url(plugins_url( '/register.iflower.php', __FILE__ )); ?>" method="post">
					<input type="hidden" name="secret_key" value="<?php echo isset($_SESSION['private_key']) ? $_SESSION['private_key'] : ''; ?>" />
					<div class="form-group">
						<label for="regName" class="col-lg-2 control-label">Nama</label>
						<div class="col-lg-10">
							<input type="text" name="regName" class="form-control" id="regName" placeholder="Nama" value="<?php echo isset($_SESSION['form_values']['regName']) ? $_SESSION['form_values']['regName'] : '' ?>" />
						</div>
					</div>
					<div class="form-group">
						<label for="regSchoolFrom" class="col-lg-2 control-label">Sekolah Asal</label>
						<div class="col-lg-10">
							<input type="text" name="regSchoolFrom" class="form-control" id="regSchoolFrom" placeholder="Sekolah Asal" value="<?php echo isset($_SESSION['form_values']['regSchoolFrom']) ? $_SESSION['form_values']['regSchoolFrom'] : '' ?>" />
						</div>
					</div>
					<div class="form-group">
						<label for="regGender" class="col-lg-2 control-label">Gender</label>
						<div class="col-lg-10">
							<label class="radio-inline">
								<input type="radio" name="regGender" id="regGender1" value="1" <?php echo isset($_SESSION['form_values']['regGender']) && $_SESSION['form_values']['regGender'] == 1 ? 'checked' : '' ?> />
								Pria </label>
							<label class="radio-inline">
								<input type="radio" name="regGender" id="regGender2" value="2" <?php echo isset($_SESSION['form_values']['regGender']) && $_SESSION['form_values']['regGender'] == 2 ? 'checked' : '' ?> />
								Wanita </label><br />
							<label class="error" for="regGender" style="display:none;">Gender wajib diisi</label>
						</div>
					</div>
					<div class="form-group">
						<label for="regBirthdata" class="col-lg-2 control-label">Tempat &amp; Tanggal Lahir</label>
						<div class="col-lg-10">
							<input type="text" name="regBirthdata" class="form-control" id="regBirthdata" placeholder="Kediri, 01 Januari 1990" value="<?php echo isset($_SESSION['form_values']['regBirthdata']) ? $_SESSION['form_values']['regBirthdata'] : '' ?>" />
						</div>
					</div>
					<div class="form-group">
						<label for="regAddress" class="col-lg-2 control-label">Alamat</label>
						<div class="col-lg-10">
							<textarea name="regAddress" class="form-control" id="regAddress" rows="5" placeholder="Alamat"><?php echo isset($_SESSION['form_values']['regAddress']) ? $_SESSION['form_values']['regAddress'] : '' ?></textarea>
						</div>
					</div>
					<div class="form-group">
						<label for="regPhone" class="col-lg-2 control-label">Phone / HP</label>
						<div class="col-lg-10">
							<input type="tel" name="regPhone" class="form-control" id="regPhone" placeholder="No telephone / HP" value="<?php echo isset($_SESSION['form_values']['regPhone']) ? $_SESSION['form_values']['regPhone'] : '' ?>" />
						</div>
					</div>
					<div class="form-group">
						<label for="regMail" class="col-lg-2 control-label">Email</label>
						<div class="col-lg-10">
							<input type="email" name="regMail" class="form-control" id="regMail" placeholder="Email" value="<?php echo isset($_SESSION['form_values']['regMail']) ? $_SESSION['form_values']['regMail'] : '' ?>" />
						</div>
					</div>
					<div class="form-group">
						<label for="regPeriodDate" class="col-lg-2 control-label">Periode</label>
						<div class="col-lg-2">
							<select name="regPeriodDate" id="regPeriodDate" class="form-control">
								<option value="0">-- Pilih Tanggal --</option>
								<option value="10">10</option>
								<option value="25">25</option>
							</select>
						</div>
						<div class="col-lg-4">
							<select name="regPeriodMonth" id="regPeriodMonth" class="form-control">
								<option value="0">-- Pilih Bulan --</option>
								<option value="01">Januari</option>
								<option value="02">Februari</option>
								<option value="03">Maret</option>
								<option value="04">April</option>
								<option value="05">Mei</option>
								<option value="06">Juni</option>
								<option value="07">Juli</option>
								<option value="08">Agustus</option>
								<option value="09">September</option>
								<option value="10">Oktober</option>
								<option value="11">November</option>
								<option value="12">Desember</option>
							</select>
						</div>
						<div class="col-lg-4">
							<select name="regPeriodYear" id="regPeriodYear" class="form-control">
								<option value="0">-- Pilih Tahun --</option>
								<?php
								
									$y = date('Y');
									
									for($i=$y;$i<=($y+1);$i++){
										
										?>
										
										<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
										
										<?php

										}
								?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="regProgram1" class="col-lg-2 control-label">Program</label>
						<div class="col-lg-8">
							<select name="regProgram1" id="regProgram1" class="form-control">
								<option value="0"> -- Pilih Program -- </option>
							</select>
						</div>
						<div class="col-sm-2">
							<p class="form-control-static">Biaya: <span id="regPrice1"></span></p>
							<input type="hidden" name="regPrice1" />
					    </div>
					</div>
					<div class="form-group">
						<label for="regProgram2" class="col-lg-2 control-label"></label>
						<div class="col-lg-8">
							<select name="regProgram2" id="regProgram2" class="form-control">
								<option value="0"> -- Pilih Program -- </option>
							</select>
						</div>
						<div class="col-sm-2">
							<p class="form-control-static">Biaya: <span id="regPrice2"></span></p>
							<input type="hidden" name="regPrice2" />
					    </div>
					</div>
					<div class="form-group">
						<label for="regCamp" class="col-lg-2 control-label">Camp</label>
						<div class="col-lg-10">
							<label class="radio-inline">
								<input type="radio" name="regCamp" id="regCamp1" value="1" />
								Ya </label>
							<label class="radio-inline">
								<input type="radio" name="regCamp" id="regCamp2" value="2" />
								Tidak </label><br />
							<label class="error" for="regCamp" style="display:none;">Camp wajib diisi</label>
						</div>
					</div>
					<div id="regCampDuration" class="form-group" style="display:none;">
						<label for="regCampDuration" class="col-lg-2 control-label">Durasi Camp</label>
						<div class="col-lg-8">
							<div class="radio">
  								<label>
									<input type="radio" name="regCampPrice" id="regCampDuration1" value="100000"> 1 Minggu / IDR 100.000,-
								</label>
							</div>
							<div class="radio">
  								<label>
									<input type="radio" name="regCampPrice" id="regCampDuration2" value="150000"> 2 Minggu / IDR 150.000,-
								</label>
							</div>
							<div class="radio">
  								<label>
									<input type="radio" name="regCampPrice" id="regCampDuration3" value="200000"> 3 Minggu / IDR 200.000,-
								</label>
							</div>
							<div class="radio">
  								<label>
									<input type="radio" name="regCampPrice" id="regCampDuration4" value="300000"> 1 Bulan / IDR 300.000,-
								</label>
							</div>
						</div>
					</div>
					<div class="form-group total-price">
						<div class="col-lg-2 control-label">
							<label>Total Biaya: </label>
						</div>
						<div class="col-lg-10">
							<p id='totalPrice' class="form-control-static">Rp 0,- </p>
						</div>
					</div>
					<div class="form-group">
						<div class="col-lg-offset-2 col-lg-10">
							<div class="g-recaptcha" data-sitekey="6LeWvCkTAAAAAHLTkOxp7nyGdAezG0QKMyDvdj5Y"></div>
						</div>
					</div>
					<div class="form-group">
						<div class="col-lg-offset-2 col-lg-10">
							<input type="submit" name="submit" class="btn btn-custom btn-lg" value="Submit" />
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>